PyTables
========

Automatically close already open hdf5 files before opening
----------------------------------------------------------

.. code-block:: python

   for hdf5 in ['hdf5_read', 'hdf5_write', 'hdf5_append']:
       if hdf5 in globals():
           globals()[hdf5].close()

Generate a ``hdf5`` file from a recarray
----------------------------------------

.. code-block:: python

   hdf5 = tables.openFile('panel.hdf5', 'w')

   filters = tables.Filters(complib='blosc', complevel=5)
   table = hdf5.createTable('/', 'panel', panel, filters=filters)


Compress and Compacting PyTables files
--------------------------------------

http://www.pytables.org/docs/manual-1.4/apc.html#ptrepackDescr

PyTables includes a handy utility called ptrepack which can be very useful not
only to compact fragmented files, but also to adjust some internal parameters
in order to use better buffer and chunk sizes for optimum I/O speed.

.. code-block:: sh

   $ ptrepack --complevel=5 --complib=blosc source.hdf5 target.hdf5

Modifying value in-place
------------------------

http://permalink.gmane.org/gmane.comp.python.pytables.user/1703

In order to modify the value,

1. Use ``cols`` accessor:

.. code-block:: python

   table.cols.number[1] = 3

2. Use ``modifyRows()`` method:

.. code-block:: python

   row = table[1]
   row['number'] = 3
   table.modifyRows(1, rows=[row])
