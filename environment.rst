=============
 Environment
=============



Mayavi
======

Mayavi to use Qt rather than wx.

On Windows PowerShell::

   [Environment]::SetEnvironmentVariable("ETS_TOOLKIT", "qt4", "User")

On GNU/Linux::

   export ETS_TOOLKIT=qt4

