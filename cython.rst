Cython
======

Passing a ``cdef`` function to another ``cdef`` function as an argument
-----------------------------------------------------------------------

http://stackoverflow.com/questions/14124049/is-there-any-type-for-function-in-cython

.. code-block:: cython

  # Define a new type for a function-type that accepts an integer and
  # a string, returning an integer
  ctypedef int (*f_type)(int, str)

  # function to be passed; the same type as f_type
  cdef int do_stuff(int a, str b):
      return 0
   
  # note the f_type type in the argument
  cdef void foo(f_type func):
      print func(0, "bar")
