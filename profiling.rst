===========
 Profiling
===========

Get the profile stats with ``cProfile``
=======================================

.. code-block:: python

   import cProfile
   prof = cProfile.Profile()
   prof.runcall(func, *args, **kwargs)

Print out the stats::

   prof.print_stats()

Dump the profile stats for GUI consumption::

   prof.dump_stats('stats.prof')


``RunSnakeRun`` to visualize the profile stats
----------------------------------------------

Install `RunSnakeRun <http://www.vrplumber.com/programming/runsnakerun/>`_ for
GUI view::

   $ pip install RunSnakeRun

It needs `wxPython <http://www.wxpython.org/>`_::

   $ conda install wxpython

Invoke ``RunSnakeRun``:

.. code-block:: sh

   $ runsnake stats.prof

* It is useful to look at ``Local`` column. This gives the time spent on a
  function itself, excluding the time spent calling other functions during the
  function call. Hence it gives the *net* time spent.

* On the other hand, ``Cum`` shows the cumulative time spent on that function,
  including the time spent for other function calls made during the function
  call.


``KCachegrind`` with ``pyprof2calltree``
========================================

Install ``KCachegrind`` and ``pyprof2calltree``::

   $ zypper in kcachegrind
   $ sudo pip install pyprof2calltree

Invoke ``KCachegrind`` through ``pyprof2calltree``::

   $ pyprof2calltree -i stats.prof -k
