========
 docopt
========

.. highlight:: python

http://docopt.org

.. code-block:: python

   Naval Fate.
    
   Usage:
     naval_fate ship new <name>...
     naval_fate ship <name> move <x> <y> [--speed=<kn>]
     naval_fate ship shoot <x> <y>
     naval_fate mine (set|remove) <x> <y> [--moored|--drifting]
     naval_fate -h | --help
     naval_fate --version
    
   Options:
     -h --help     Show this screen.
     --version     Show version.
     --speed=<kn>  Speed in knots [default: 10].
     --moored      Moored (anchored) mine.
     --drifting    Drifting mine.


Positional argument
===================

.. code-block:: python

   Usage: my_program <host> <port>

Words starting with ``<``, ending with ``>`` or upper-case words are interpreted
as positional arguments.

Options
=======

.. code-block:: python

   -o --option

Words starting with one or two dashes (with exception of ``-``, ``--`` by
themselves) are interpreted as short (one-letter) or long options,
respectively.

Command
=======

All other words that do not follow the above conventions of ``--options`` or
``<arguments>`` are interpreted as (sub)commands.

