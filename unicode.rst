.. highlight:: python

=========
 Unicode
=========


Remove unicode characters from a string
=======================================

.. code:: python

   def removeNonAscii(s):
       return "".join(i for i in s if ord(i) < 128)

Also, You can filter all characters from the string that are not printable
using ``string.printable``
