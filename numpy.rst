Numpy
=====

Find indices of duplicates
--------------------------

.. code-block:: python

   #----------------------------------------------------------------------
   def duplicate(arr):
       '''
       return indices of duplicated entries of a ndarray
       '''
    
       ix_dup = ones((arr.shape[0], ), dtype=bool)
       ix_dup[unique(arr, True)[1]] = False
    
       ix_return = []
    
       for ix in where(ix_dup)[0]:
           ix_return.append(tuple(where(arr==arr[ix])[0]))
    
       return(ix_return)

