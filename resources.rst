===========
 Resources
===========

* `The Hitchhiker’s Guide to Python!
  <http://docs.python-guide.org/en/latest/>`_
* `Python Testing Frameworks <http://pythontesting.net/start-here/>`_
