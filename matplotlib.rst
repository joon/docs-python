============
 matplotlib
============

..highlight:: python

``mpltools``: Tools for Matplotlib
==================================

http://tonysyu.github.io/mpltools/

``mpltools`` provides tools for Matplotlib that make it easier to adjust the
style, choose colors, make specialized plots, etc.




dates
=====

.. code-block:: python

   # date formatting
   years = mdates.YearLocator()   # every year
   yearsFmt = mdates.DateFormatter('%Y', interval=1)
    
   months = mdates.MonthLocator()  # every month
   monthsFmt = mdates.DateFormatter('%m', interval=1)
    
   weeks = mdates.WeekdayLocator(byweekday=SU)  # every weekday
   weeksFmt = mdates.DateFormatter('%Y-%m-%d', interval=1)

   # format the ticks
   ax.xaxis.set_major_locator(months)
   ax.xaxis.set_major_formatter(monthsFmt)
   ax.xaxis.set_minor_locator(weeks)

LaTeX
=====

.. code-block:: python

   import matplotlib.pyplot as plt
   from matplotlib import rc
    
   rc('text', usetex=True)
    
   plt.sunplot(111)
    
   plt.text(0.05, 0.90, r'\underline{\alpha}: ', fontsize=12)

Subplots
========

.. code-block:: python

   shape = (1, 1)
   figsize = (10, 6)
   fig, axs = plt.subplots(*shape, 
                           sharex=False, sharey=False,
                           figsize=figsize, 
                           squeeze=False)

   ax = axs[0, 0]
   ax.plot(xdata, ydata, label='label', color='blue', linestyle='-')
   ax.set_title('subplot title')
   ax.set_xlabel('xlabel')
   ax.set_ylabel('ylabel')
   ax.legend(loc='lower right')

   fig.suptitle('figure title')
   fig.tight_layout()  # narrow margin

Save to PDF
===========

.. code-block:: python

   from matplotlib.backends.backend_pdf import PdfPages
   pdffile = '{}.pdf'.format('path')
   pp = PdfPages(pdffile)
   # pp.savefig can be called multiple times to save to multiple pages
   pp.savefig()
   pp.close()
