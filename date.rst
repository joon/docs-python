date
====

`Python's strftime directives <http://strftime.org/>`_

Get week number
---------------

.. code-block:: python
   
   import datetime
   today = datetime.date.today()
   print(today.strftime("%U"))
