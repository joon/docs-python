.. Python documentation master file, created by
   sphinx-quickstart on Wed Apr 10 14:50:03 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 1

   environment
   cython
   date
   docopt
   matplotlib
   numpy
   profiling
   pytables
   unicode
   web-crawling
   resources


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

